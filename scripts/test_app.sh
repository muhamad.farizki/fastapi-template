#!/bin/sh
. scripts/load_env.sh \
&& flake8 \
&& mamba app --enable-coverage \
&& coverage report -m