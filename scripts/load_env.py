import os
import sys
import yaml

env = os.environ.get('API_ENV', None)
if(not env):
    sys.exit(0)
config_file = open('config.yaml', 'r')
config = yaml.safe_load(config_file)
config_file.close()

for key, val in config[env].items():
    print('export {}={}'.format(key, val))
