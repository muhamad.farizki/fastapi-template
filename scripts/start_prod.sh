#!/bin/sh
cd alembic
alembic upgrade head
cd ..
gunicorn -k uvicorn.workers.UvicornWorker -c gunicorn_conf.py app:app