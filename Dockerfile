FROM python:3.7.8-alpine3.11

RUN apk add --no-cache --virtual .build-deps gcc libc-dev make curl \
libffi-dev openssl-dev postgresql-dev musl-dev

RUN pip install --no-cache-dir poetry 

COPY ./ /app
WORKDIR /app/

RUN poetry export -f requirements.txt --dev > requirements.txt && pip install -r requirements.txt

RUN chmod +x scripts/*

ENTRYPOINT [ "/bin/sh", "scripts/test_app.sh" ]
